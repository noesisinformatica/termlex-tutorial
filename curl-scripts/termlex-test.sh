#!/bin/bash
echo 'These scripts are designed to access version 2 of Termlex'
# Verify that jq is installed!
if ! type "jq" > /dev/null; then
  # Notify user
  echo "This script requires Jq but it's not installed. Please install from https://stedolan.github.io/jq/download/"
fi

# Function that displays response + response times for each test
function curl_get {
  # Use command below to just display reponse times without showing any responses
  # curl -o /dev/null  -s -w "Connect time: %{time_connect} + Transfer time: %{time_starttransfer} = Total time: %{time_total} for URL: \n$1\n" "$1"
  # Use command below to display both responses and response times
  curl -s -w "\nConnect time: %{time_connect} + Transfer time: %{time_starttransfer} = Total time: %{time_total}\n\n" "$1"
}

# Function that displays response + response times for each test
function curl_getWithoutLog {
  # Use command below to display both responses and response times
  curl "$1"
}

# Function that displays response + response times for each test
function curl_del {
  argCount=$#;
  if(( $# == 2 )); then
    curl -s -w "\nConnect time: %{time_connect} + Transfer time: %{time_starttransfer} = Total time: %{time_total}\n\n" -H "Content-Type: application/json" -X DELETE "$1" -d "$2"
  else
    curl -s -w "\nConnect time: %{time_connect} + Transfer time: %{time_starttransfer} = Total time: %{time_total}\n\n" -H "Content-Type: application/json" -X DELETE "$1"
  fi
}

# Function that displays response + response times for each test
function curl_post {
  # Use command below to display both responses and response times
  curl -s -w "\nConnect time: %{time_connect} + Transfer time: %{time_starttransfer} = Total time: %{time_total}\n\n" \
    -H "Content-Type: application/json" -X POST "$1" -d "$2"
}

# Function that displays response + response times for each test
function curl_postWithoutLog {
  curl -H "Content-Type: application/json" -X POST "$1" -d "$2"
}

# Function that displays response + response times for each test
function curl_put {
  # Use command below to just display reponse times without showing any responses
  # curl -o /dev/null  -s -w "Connect time: %{time_connect} + Transfer time: %{time_starttransfer} = Total time: %{time_total} for URL: \n$1\n" "$1"
  # Use command below to display both responses and response times
  curl -s -w "\nConnect time: %{time_connect} + Transfer time: %{time_starttransfer} = Total time: %{time_total}\n\n" \
    -H "Content-Type: application/json" -X PUT "$1" -d "$2"
}

# Replace variables below with relevant values - particularly the baseUrl
baseUrl=http://localhost:7221
miConceptId=22298006
mastectomyConceptId=69031006
searchToken=ast
username="yourtermlexusernamehere"
password="yourtermlexpasswordhere"

## AUTHENTICATION
# Login using your user name and password to retrieve token. We then use this token for all other calls.
token=$(curl -H "Content-Type: application/json" -XPOST "$baseUrl/authenticate" -d'
{
    "username": $username,
    "password": $password,
    "rememberMe": "false"
}' | jq -r ".id_token")

printf "\nReceived token : $token \n"

## CONCEPT LOOKUP TEST SCRIPTS ###

# Get concept details for id 22298006 - Myocardial infarction
curl_get $baseUrl/concepts/$miConceptId
# Get full concept details for id 22298006 - Myocardial infarction
curl_get $baseUrl/concepts/$miConceptId?flavour=ID_DESCRIPTIONS_RELATIONSHIPS
# Get SNOMED CT's preferred term (term) in Swedish language for given concept id - asthma
curl_get $baseUrlconcepts/195967001/pt?language=sv
# Get normal form for concept for id 22298006 - Myocardial infarction , rendered in compositional grammar syntax - SNOMED CT's official syntax
curl_get $baseUrl/concepts/$miConceptId/cgf
# Compare two concepts for parent child relationship (child concept is asthma)
curl_get $baseUrlconcepts/compare?conceptId1=64572001&conceptId2=195967001

## HIERARCHY LOOKUP TEST SCRIPTS ###

# Return children for diabetes mellitus
curl_get $baseUrl/hierarchy/73211009/children
# Return descendants for diabetes mellitus - recursive expansion of entire tree under diabetes
curl_get $baseUrl/hierarchy/73211009/descendants
# Return parents for myocardial infarction - concepts in SNOMED CT can hve more than one parent, so it should return all parents
curl_get $baseUrl/hierarchy/$miConceptId/parents
# Return children of myocardial infarction - rendered as a list of ids with labels and associated child count - Browser functionality
curl_get $baseUrl/hierarchy/$miConceptId/childHierarchy
# Return parents of myocardial infarction - rendered as a list of ids with labels and associated child count - Browser functionality
curl_get $baseUrl/hierarchy/$miConceptId/parentHierarchy


## DEFINITION/QUERY TEST SCRIPTS ##

# Create a constraint via post
constraintId=$(curl_post $baseUrl/constraints/$mastectomyConceptId | jq -r '.id')
echo "Newly created constraint id : $constraintId"
# Get subsumption of newly created constraint
echo "Get subsumption of newly created constraint"
curl_get $baseUrl/constraints/$constraintId/flavour
# Update subsumption of newly created constraint to flavour ANY_TYPE_OF_BUT_NOT_SELF
echo "Update subsumption of newly created constraint to flavour ANY_TYPE_OF_BUT_NOT_SELF"
curl_put $baseUrl/constraints/$constraintId/flavour "ANY_TYPE_OF_BUT_NOT_SELF"
# save constraint for use later on
constraint=$(curl_getWithoutLog $baseUrl/constraints/$constraintId)
echo "Saved constraint : $constraint"
# Create a component query
#curl_post $baseUrl/queries
definitionId=$(curl_post $baseUrl/queries | jq -r '.uuid')
echo "Newly created definition id : $definitionId"
# Print uuids of all existing definitions
echo "Print uuids of all existing definitions"
curl_get $baseUrl/queries | jq -r '.[].uuid'
# Set included constraint for component query
#curl_post $baseUrl/queries/$definitionId/constraints/included $constraint
#echo "Printing included constraint for component query with id : $definitionId"
#curl_get $baseUrl/queries/$definitionId/constraints/included

curl_put $baseUrl/queries/$definitionId/constraints/included?includedId=$mastectomyConceptId
echo "Printing included constraint for component query with id : $definitionId"
curl_get $baseUrl/queries/$definitionId/constraints/included
# Create excluded constraints with concepts 237378001 |incisional biopsy of breast|  , 237372000 |excisional biopsy of breast|
excludedConstraint1=$(curl_postWithoutLog $baseUrl/constraints/237378001)
excludedConstraint2=$(curl_postWithoutLog $baseUrl/constraints/237372000)
echo "Constraints to be posted  : [$excludedConstraint1, $excludedConstraint2]"
# Set excluded constraints for component query
curl_post $baseUrl/queries/$definitionId/constraints/excluded "[$excludedConstraint1, $excludedConstraint2]"
# print excluded constraints, must show two definitions
exludedDefinitionIds=$(curl_get $baseUrl/queries/$definitionId/constraints/excluded | jq -r '.[].id')
echo "2 excluded ids must be shown : $exludedDefinitionIds"
# Add some more excluded constraints using just ids - 59620004 |mastectomy for gynaecomastia|, 447421006 |prophylactic mastectomy|
curl_put $baseUrl/queries/$definitionId/constraints/excluded "[59620004, 447421006]"
# print excluded constraints, must show four definitions
exludedDefinitionIds=$(curl_get $baseUrl/queries/$definitionId/constraints/excluded | jq -r '.[].id')
echo "4 excluded ids must be shown : $exludedDefinitionIds"
# Delete exluded constraint
curl_del $baseUrl/queries/$definitionId/constraints/excluded "[447421006]"
# print excluded constraints, must show three definitions
exludedDefinitionIds=$(curl_get $baseUrl/queries/$definitionId/constraints/excluded | jq -r '.[].id')
echo "Only 3 excluded ids must be shown : $exludedDefinitionIds"

## REFSET TEST SCRIPTS ##

# Create new refset with random name
refsetId=$(curl_post $baseUrl/refsets "{\"name\": \"Test refset $RANDOM\"}" | jq -r '.id')
echo "Newly created refset id : $refsetId"
# Now use refset id to set members -- setting a collection is a POST
curl_post $baseUrl/refsets/$refsetId/members '[233832000, 418044006, 703212004, 50043002]'
# Now print refset members
curl_get $baseUrl/refsets/$refsetId/members
# Add new member to refset - add member is a PUT method
curl_put $baseUrl/refsets/$refsetId/members '[418092006]'
# Now print refset members - 418092006 should have been added
curl_get $baseUrl/refsets/$refsetId/members
# Remove member from refset
curl_del $baseUrl/refsets/$refsetId/members '[233832000]'
# Now print refset members - 233832000 should have been removed
curl_get $baseUrl/refsets/$refsetId/members
curl_get $baseUrl/refsets/$refsetId/members/count
# Now add definitions to refset
echo "add definition with id $definitionId to refset"
curl_post $baseUrl/refsets/$refsetId/definitions "[\"$definitionId\"]"
# Now get members which should return types of mastectomy without previous exclusions
curl_get $baseUrl/refsets/$refsetId/members
curl_get $baseUrl/refsets/$refsetId/members/count
echo "Create a union expression using just concept ids - 254837009 and 429087003"
# Create a union expression using just concept ids - 254837009 |malignant tumor of breast| , 429087003 |history of malignant neoplasm of breast |
unionExpressionId=$(curl_post $baseUrl/queries/union '[254837009, 429087003]' | jq -r '.uuid')
echo "Union expression created id : $unionExpressionId"
existingMemberCount=$(curl_get $baseUrl/refsets/$refsetId/members/count)
# Now we add the union expression and concept 429579007 |radiotherapy to axilla| as defintions to refset - can pass combo of uuid and conceptIds
echo "Add the union expression and concept 429579007 as definitions"
curl_put $baseUrl/refsets/$refsetId/definitions "[\"$unionExpressionId\", \"429579007\"]"
#echo "Getting updated members of refset"
curl_get $baseUrl/refsets/$refsetId/members
updatedMemberCount=$(curl_get $baseUrl/refsets/$refsetId/members/count)
echo "Member count before adding union expression and concept: $existingMemberCount"
echo "Updated member count : $updatedMemberCount"

## Validation TEST SCRIPTS ##

# Return only valid children of Myocardial infarction
curl_post $baseUrl/validate/concept/22298006 '[233832000, 418044006, 703212004, 50043002]'
# Return valid children of respiratory disease - only last 4 concepts are valid
curl_post $baseUrl/validate/concept/50043002 '[233832000, 418044006, 703212004, 418092006, 127275008, 111273006, 421092003]'
# Validate differents ids against refset created above - first 3 are not valid
curl_post $baseUrl/validate/refset/$refsetId '[233832000, 418044006, 703212004, 6189002, 59620004, 41104003, 237368004, 429087003, 93884005, 286893002]'
# Validate differents ids against query expression created above - first 3 are not valid
curl_post $baseUrl/validate/query/$unionExpressionId '[233832000, 418044006, 703212004, 448451002, 93884005, 286893002]'
# Validate differents ids against constraint created above - first 3 are not valid
curl_post $baseUrl/validate/constraint/$constraintId '[233832000, 418044006, 703212004, 237368004, 72577009, 14714006, 59620004]'


## SEARCH TEST SCRIPTS ###

# Get matches for token 'ast' - returns first 100 results
curl_get $baseUrl/search/sct/?term=$searchToken
# Showing suggestions for misspelling of diabetesT (notice the T at the end)
curl_get $baseUrl/search/sct?term=diabetest%20melli
# Get matches for token 'mastec' for refset created above - returns first 100 results
refsetSearchTerm=mastec
curl_get $baseUrl/search/refset/?includedRefsetIds=$refsetId\&term=$refsetSearchTerm