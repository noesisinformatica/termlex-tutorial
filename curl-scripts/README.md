# Termlex How to Use Guide

Introduction

## What is Termlex?
Termlex is a RESTful terminology server that is designed to ,....

## Authentication
Before you start, you will need to create an account for Termlex. You can do this by:
Go to https://api.termlex.com/register and create an account.
Once you have confirmed your account via email, you will be able to login to Termlex on the website.
After you log in, navigate to `Account` --> `Settings` page.
On this page, click on the `Generate API token` button. Termlex will generate an API token for you.
Copy this API key for use later on in the tutorial

Warning: Make sure you have the correct API key before you try any of the other sections.

## How to search for matches in Termlex

Warning: Make sure you have the correct API key before you try any of the other sections. Set the `Bearer token` in the cUrl headers before you try the example.

### Get matches for token àst
In this example, we will search all of SNOMED CT content for matches for token `ast`. Termlex makes searching all of SNOMED CT trivial. To search for matches for `ast', try the `curl` command:
> - Get matches for token 'ast' - returns first 100 results
>    - `curl https://api.termlex.com/search/sct/?term=ast`
> - The general pattern for search is below, where $baseUrl is the URL of the Termlex instance and `$searchToken`is the query token
>    - `curl $baseUrl/search/sct/?term=$searchToken`
Now let us analyse this query to understand what happened.
- First Termlex recognised that the search needed to be performed against SNOMED CT based on the URL of the GET query.
- Since we did not specify the `edition` and `version` of SNOMED CT to search against, it executed the search against the `default` version set up in Termlex. This is a useful fallback mechanism when you do not want to do a quick search. You can find ask Termlex for the `default version` information by visiting https://api.termlex.com/defaultVersion.
- Since we did not specify the number of matches to be returned, we were presented with the first 100 matches.
- The results is an array of objects, with addition information about `total number of matches', `number of categories`, etc.

### Get matches for partial words
> - Get matches for partial words - aka incremental search
>    - `curl https://api.termlex.com/search/sct/?term=diabett%20melli`
- This query is almost the same as above, except we have used partial phraes to get matches. So instead of searching for `diabetes mellitus`, we specifed the search token as `diabetest%20melli`. Notice the url encoded format of `diabe melli`.
- As in the case above, Termlex makes assumptions about other search parameters and returns matches as an array of objects.

## How to retrieve/expand concept hierarchies in Termlex

As you probably know by now, SNOMED CT's concepts/codes are organised as hierarchies. In order words, SNOMED CT is organised as a directed acyclical graph, where nodes in the graph are connected to each other. One of the common uses cases for a terminology server is to access children or parents of a given concept/code. So given the hierarchy of concepts below, the assocations between the concepts are given specific names in SNOMED CT.
```
        [A]
      /    \
    /       \
  [B]       [C]
           /  \
          /     \
        [D]     [E]
```
|Reference Node | Relationship name | Nodes                |
|:--------------|:------------------|:---------------------|
|[A]            | Children          | [B] and [C]          |
|[C]            | Children          | [D] and [E]          |
|[B]            | Parents           | [A]                  |
|[E]            | Parents           | [C]                  |
|[A]            | Descendants       | [B], [C], [D] and [E]|
|[D]            | Ancestors         | [C] and [A]          |

### Get children for a given concept
This call is useful when you want to only want to first level of child relationships for a given concept. In general this is useful when rendering a heirarchy.
> - Return children for diabetes mellitus
>    - `curl_get $baseUrl/hierarchy/73211009/children`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance and $conceptId is the `id` of the concept
>    - `curl_get $baseUrl/hierarchy/$conceptId/children`
> - Return children for diabetes  with labels
>    - `curl_get $baseUrl/hierarchy/73211009/childHierarchy`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance and $conceptId is the `id` of the concept
>    - `curl_get $baseUrl/hierarchy/$conceptId/childHierarchy`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.

### Get parents for a given concept
This call is useful when you want to only want to first level of parent relationships for a given concept. In general this is useful when rendering a heirarchy.
> - Return parents for diabetes mellitus
>    - `curl_get $baseUrl/hierarchy/73211009/parents`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance and $conceptId is the `id` of the concept
>    - `curl_get $baseUrl/hierarchy/$conceptId/parents`
> - Return parents for diabetes  with labels
>    - `curl_get $baseUrl/hierarchy/73211009/parentHierarchy`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance and $conceptId is the `id` of the concept
>    - `curl_get $baseUrl/hierarchy/$conceptId/parentHierarchy`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.
- Note that concepts in SNOMED CT can have more than one parent. So this call returns all parents.

### Get descendants for a given concept
This call is useful when you want all hierarchy of a concept including its children, grandchildren, and so on. This call is useful when doing validations or querying. For example, in order to retrieve all patients with any heart disease, you would retrieve all descendants of heart disease and then select any patient records that contain any of the descendants.
> - Return descendants for diabetes mellitus
>    - `curl_get $baseUrl/hierarchy/73211009/descendants`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance and $conceptId is the `id` of the concept
>    - `curl_get $baseUrl/hierarchy/$conceptId/descendants`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.

### Get ancestors for a given concept
This call is useful when you want all parent hierarchy of a concept including its parents, grandparents, and so on. This call is useful when doing validations.
> - Return ancestors for diabetes mellitus
>   - `curl_get $baseUrl/hierarchy/73211009/ancestors`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance and $conceptId is the `id` of the concept
>    - `curl_get $baseUrl/hierarchy/$conceptId/ancestors`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.

## Looking up concepts in Termlex
Termlex has a collection of endpoints that can be used to lookup and retrieve entire concepts or attributes of a given concept. This section describes some of the more common endpoints.

### Get a concept with given id
> - Get concept details for id 22298006 - Myocardial infarction
>   - `curl https://api.termlex.com/concepts/22298006`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance and $conceptId is the `id` of the concept
>    - `curl_get $baseUrl/concepts/$conceptId`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.
- By default Termlex only returns minimal information about a concept, like its `id`, its `label` also called the `preferred term`, the `type`, its status (as boolean indication whether it is active). This is a deliberate design decision because most of the time we are not interested in all the `descriptions` and `relationships` associated with a concept. So by excluding this information, Termlex reduces the bandwidth usage, cpu cycles and improves performance. If you do want to retrive more information about a concept, you can do so by specifying the `ConceptFlavour` as a parameter in the call as show below.

### Get full details including descriptions, relationships, etc for a concept with given id
> - Get concept details for id 22298006 - Myocardial infarction
>   - `curl https://api.termlex.com/concepts/22298006?flavour=ID_DESCRIPTIONS_RELATIONSHIPS`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance, $conceptId is the `id` of the concept and $conceptFlavour is the flavour that determines level of concept details retrieved.
>   - `curl_get $baseUrl/concepts/$conceptId?flavour=$conceptFlavour`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.

#### Concept flavour
There are different levels of `Concept flavour` defined in Temrlex. They are listed below in increasing order or details that they retrieve.

Concept Flavour     | Concept details included
ID_LABEL            | Minimal, with just id, concept type, status and preferred term
ID_DESCRIPTIONS     | As above but also returns descriptions associated with concept as preferredTerm, fullySpecifiedName and synonyms as an array
ID_DESCRIPTIONS_RELATIONSHIPS | As above but now includes full details of all descriptions, including all their attributes like ids and full details of all relationshps associated with concept
ID_DESCRIPTIONS_STATED_RELATIONSHIPS | As above but now includes full details of all descriptions, including all their attributes like ids and full details of only stated relationshps associated with concept

The user is given the responsibility for deciding the appropriate `Concept Flavour` for their use case.
- For example to retrieve all synonyms of a given concept, we could use the ID_DESCRIPTIONS flavour and retrieve the `synonyms` attribute from the response.
- On the otherhand, to return all relationships associated with a concept, we would have to use the ID_DESCRIPTIONS_RELATIONSHIPS and retrieve the `relationships` attribute on the response.

### Get the preferred term for a concept in a given edition
> - Get preferred term for id 22298006 - Myocardial infarction in the UK April 2016 edition
>    - `curl https://api.termlex.com/version/20160401/concepts/22298006/pt`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance, $conceptId is the `id` of the concept and $versionId is the edition to lookup expressed as a YYYYmmdd string.
>    - `curl_get $baseUrl/version/$versionId/concepts/$conceptId/pt`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.
- Note that while it is possible to use `language` as a proxy for edition, it is not recommended.

### Get the normal form of a concept rendered in compositional grammar
> - Get normal form for concept for id 22298006 - Myocardial infarction , rendered in compositional grammar syntax - SNOMED CT's official syntax
>    - `curl https://api.termlex.com/version/20160401/concepts/22298006/cgf`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance, $conceptId is the `id` of the concept and $versionId is the edition to lookup expressed as a YYYYmmdd string.
>    - `curl_get $baseUrl/version/$versionId/concepts/$conceptId/cgf'
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.

### Compare two concepts to see if they are equivalent or if one subsumes the other
> - Compare two concepts for parent child relationship (child concept is asthma)
>    - `curl https://api.termlex.com/version/20160401/concepts/compare?conceptId1=64572001&conceptId2=195967001`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance, $conceptId1 and $conceptId2 are the `id`s of the concepts being compared and $versionId is the edition to lookup expressed as a YYYYmmdd string.
>    - `curl_get $baseUrl/version/$versionId/concepts/$conceptId/compare?conceptId1=$conceptId1&conceptId2=$conceptId2`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.

# Advanced Features - Constraints, Queries and Refsets

While in most cases it is possible to lookup concepts and hierarchies directly, sometimes lookups are a lot more complicated than just operations on single concept. Termlex makes it easy to support these complex real world use cases by implementing a `query model`. In order to understand this query model, let us explore this real world query:

Return all patients who were diagnosed with `Breast cancer` and underwent subsequent `Mastectomy` and `axiallary radiotherapy`.

For those of us who have been involved in some form of clinical trials, this is a simple cohort selection criteria. In fact this criteria can be re-expressed more formally as a set of logical statements.
Return all patients
      {who have `Breast Cancer`                     # statement 1
            AND
            who underwent `Mastectomy`              # statement 2
            AND
            who underwent `Axillary radiotherapy`   # statement 3
      }

### Get matches for term in a specified refset
> - Get matches for token 'ast' - returns first 100 results
>   - `curl https://api.termlex.com/refset/xxxxxxxx/search/?term=ast`
> - The general pattern for search is below, where $baseUrl is the URL of the Termlex instance, $refsetId is the `id` of the refset and `$searchToken`is the query token
>   - `curl $baseUrl/refsets/$refSetId/search/?term=$searchToken`
