## Termlex Curl Tutorial
Termlex is a modular, service oriented API for SNOMED CT. This repo contains various `curl` scripts designed to understand how to use Termlex.

The main reference is viewable at [Termlex API page](https://termlex.gitlab.io). This repo contains `curl` scripts for accessing Termlex.

#### Authors
  * Jay Kola

#### License
  * [Noesis Informatica License](https://noesisinformatica.com/licenses/LICENSE-1.0.txt)

### Tutorial Resources
The following is a list of resources available for Termlex:

|Resource name | Description | Location |
|:------------- |:-------------:|:-----|
`termlex-test.sh` | `curl` based test scripts that show how to interact with Termlex | [https://gitlab.com/noesisinformatica/termlex-tutorial/blob/master/curl-scripts/termlex-test.sh](https://gitlab.com/noesisinformatica/termlex-tutorial/blob/master/curl-scripts/termlex-test.sh) |
`Angularjs demo` | `AngularJs` based demonstrator app that shows how to interact with Termlex  - separate repo| [https://gitlab.com/termlex/angularjs-demo](https://gitlab.com/termlex/angularjs-demo) |

#### Test categories
There are various types of tests provided in the  `termlex-test.sh` script.

|Test category name | Description |
|:------------- |:------------- |
`Concept tests` | Tests designed to access concept resources |
`Hierarchy tests` | Tests designed to access concept hierarchies |
`Refset tests` | Tests designed to access refset resources |
`Query tests` | Tests designed to access queries resources |
`Constraint tests` | Tests designed to access constraint resources |
`Validation tests` | Tests designed to demonstrate validation against above named resources |
`Search tests` | Tests designed to demonstrate searching above named resources |

#### Running test/tutorial locally
This procedure applies only if you are running a local instance of Termlex. You do not need this if you are accessing the remote instance.

1. Extract the `Termlex.zip` to a folder where you have read and write permissions.
2. Verify the entries in `snomed-db.properties` to match your MySQL database that contains SNOMED CT data.
3. Set database connection properties in `termlogic-server.properties` for Termlex to store data. 
You can also edit this file to change the port that Termlex runs on.
4. Start Termlex using the `run.sh` or `run.bat` files in the folder.
5. Use the scripts and utilities provided in this module to learn how to use Termlex

Happy Hacking!
