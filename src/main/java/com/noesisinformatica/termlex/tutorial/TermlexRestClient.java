package com.noesisinformatica.termlex.tutorial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A simple REST client that communicates with Termlex API.
 */
@Service
public class TermlexRestClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(TermlexRestClient.class);
    private String serverUrl = "http://localhost:7221/";
    @Autowired
    private RestTemplate restTemplate;
    private HttpHeaders headers;

    public TermlexRestClient() {
        this.restTemplate = new RestTemplate();
        this.headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        // verify server url
        setServerUrl(serverUrl);
    }

    public ResponseEntity get(String uri) {
        HttpEntity<String> requestEntity = new HttpEntity<>("", headers);
        return restTemplate.exchange(serverUrl + uri, HttpMethod.GET, requestEntity, String.class);
    }

    public ResponseEntity post(String uri, String json) {
        HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);
        return restTemplate.exchange(serverUrl + uri, HttpMethod.POST, requestEntity, String.class);
    }

    public ResponseEntity put(String uri, String json, Class clazz) {
        HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);
        return restTemplate.exchange(serverUrl + uri, HttpMethod.PUT, requestEntity, clazz);
    }

    public ResponseEntity delete(String uri, Class clazz) {
        HttpEntity<String> requestEntity = new HttpEntity<>("", headers);
        return restTemplate.exchange(serverUrl + uri, HttpMethod.DELETE, requestEntity, clazz);
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(serverUrl).openConnection();
            connection.setRequestMethod("HEAD");
            int responseCode = connection.getResponseCode();
            if (responseCode != 200) {
                LOGGER.error("Error connecting to serverUrl specified :" + serverUrl);
            }
        } catch (IOException e) {
            LOGGER.error("Error creating connection to server. Nested exception is : ", e);
        }
    }
}
