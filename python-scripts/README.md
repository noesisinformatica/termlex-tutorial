# Termlex How to Use Guide for Python Developers

Introduction

## What is Termlex?
Termlex is a RESTful terminology server that is designed to ,....

### System set up

Since you are a python developer, we assume you have the following tools installed on your system.
 - Python 3
 - Pip

Once you have both installed, you can install `Termlex Python Client` using `pip`. To install it simply execute the following command from your terminal.

> python -m pip install git+https://gitlab.com/termlex/python-client.git

This should automatically download and install the latest version of the Termlex python library, which makes it super easy to access Termlex functionality in your Python applications.

### Termlex credentials
Before you start, you will need to create an account for Termlex. You can do this by:
Go to https://api.termlex.com/register and create an account.
Once you have confirmed your account via email, you will be able to login to Termlex on the website.

You will need this username and password for accessing Termlex API via an API.

## Authentication
In this example, we will read you Termlex account credentials via environment variables
 - TLX_USERNAME
 - TLX_PASSWORD

We will assume you know how to set evironment variables for your operating system. Now with that out of the way, lets get started!

You can use a the same code below to retrieve your `API Key` using the credentials set above. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
import os

## Read TLX_USERNAME and TLX_PASSWORD variables set in environment variables
tlx_username = os.environ['TLX_USERNAME']
tlx_password = os.environ['TLX_PASSWORD']


## First configure the client library to communicate with an instance of Termlex
configuration = swagger_client.Configuration()
configuration.host = 'https://uat.termlex.com'
api_client = swagger_client.ApiClient(configuration)

# create an instance of the AccountResource API, to get token
api_instance = swagger_client.AccountresourceApi()
login_dto = swagger_client.LoginDTO() # LoginDTO | loginDTO
login_dto.username = tlx_username
login_dto.password = tlx_password

# Attempt to get API key
try: 
    # authorize
    api_response = api_instance.authorize_using_post(login_dto)
    key = api_response['id_token']
except ApiException as e:
    print("Exception when calling AccountresourceApi->authorize_using_post: %s\n" % e)

```

> Notice how we retrieved our `API key` using the call above. We can now reuse this `API Key`.

Let us now use the key to simply verify our account details using the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
# Now set key as header value for all further access
print("\nUsing API Key: %s\n" %key) // key retrieved in above step
api_client = swagger_client.ApiClient(header_name='Authorization', header_value='Bearer '+key)

# Getting account details
api_instance = swagger_client.AccountresourceApi(api_client)

try: 
    # getAccount
    api_response = api_instance.get_account_using_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AccountresourceApi->get_account_using_get: %s\n" % e)
```

Now lets move on to more useful functionality in Termlex.

## Looking up concepts in Termlex
Termlex has a collection of endpoints that can be used to lookup and retrieve entire concepts or attributes of a given concept. This section describes some of the more common endpoints.

### Get a concept with given id

Let us now use the key to access a concept in SNOMED CT details using the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

> **Task** Get concept details for concept with id 22298006 - Myocardial infarction

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
api_instance = swagger_client.ConceptresourceApi(api_client)
concept_id = 22298006 # int | id The concept id
version = 'null' # str | The version of the terminology product as date string
flavour = 'ID_ONLY' # str | The flavour of the concept - specifies details retrieved (optional)

try: 
    # get the concept with given id
    api_response = api_instance.get_concept_for_id_using_get(concept_id, version, flavour=flavour)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConceptresourceApi->get_concept_for_id_using_get: %s\n" % e)
```

- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.
- By default Termlex only returns minimal information about a concept, like its `id`, its `label` also called the `preferred term`, the `type`, its status (as boolean indication whether it is active). This is a deliberate design decision because most of the time we are not interested in all the `descriptions` and `relationships` associated with a concept. So by excluding this information, Termlex reduces the bandwidth usage, cpu cycles and improves performance. If you do want to retrive more information about a concept, you can do so by specifying the `ConceptFlavour` as a parameter in the call as show below.

### Get full details including descriptions, relationships, etc for a concept with given id
> **Task** Get concept details for id 22298006 - Myocardial infarction, with different levels of details

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
api_instance = swagger_client.ConceptresourceApi(api_client)
concept_id = 22298006 # int | id The concept id
version = 'null' # str | The version of the terminology product as date string
flavour = 'ID_DESCRIPTIONS_RELATIONSHIPS' # str | The flavour of the concept - specifies details retrieved (optional)

try: 
    # get the concept with given id
    api_response = api_instance.get_concept_for_id_using_get(concept_id, version, flavour=flavour)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConceptresourceApi->get_concept_for_id_using_get: %s\n" % e)
```

> - The generic form of this query is below, where `concept_id` is the `id` of the concept and `flavour` is the flavour that determines level of concept details retrieved.
>   - `api_instance.get_concept_for_id_using_get(concept_id, version, flavour=flavour)`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.

#### Concept flavour
There are different levels of `Concept flavour` defined in Temrlex. They are listed below in increasing order or details that they retrieve.

|Concept Flavour     | Concept details included|
|:--------------|:------------------|
|ID_LABEL            | Minimal, with just id, concept type, status and preferred term|
|ID_DESCRIPTIONS     | As above but also returns descriptions associated with concept as preferredTerm, fullySpecifiedName and synonyms as an array|
|ID_DESCRIPTIONS_RELATIONSHIPS | As above but now includes full details of all descriptions, including all their attributes like ids and full details of all relationshps associated with concept|
|ID_DESCRIPTIONS_STATED_RELATIONSHIPS | As above but now includes full details of all descriptions, including all their attributes like ids and full details of only stated relationshps associated with concept|

The user is given the responsibility for deciding the appropriate `Concept Flavour` for their use case.
- For example to retrieve all synonyms of a given concept, we could use the ID_DESCRIPTIONS flavour and retrieve the `synonyms` attribute from the response.
- On the otherhand, to return all relationships associated with a concept, we would have to use the ID_DESCRIPTIONS_RELATIONSHIPS and retrieve the `relationships` attribute on the response.

### Get the preferred term for a concept in a given edition
> **Task** Get preferred term for id 22298006 - Myocardial infarction in the UK April 2016 edition

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
api_instance = swagger_client.ConceptresourceApi(api_client)
id = 22298006 # str | id
language = 'en' # str | language
version = 20160401 # str | The version of the terminology product as date string

try:
    # get the Preferred Term of concept with given id
     api_response = api_instance.get_concept_preferred_term_for_id_using_get(id, language, version)
     pprint(api_response)
except ApiException as e:
     print ("Exception when calling ConceptresourceApi->get_concept_preferred_term_for_id_using_get: %s\n" % e)
```

> - The generic form of this query is below, where `concept_id` is the `id` of the concept and `version` is the edition to lookup expressed as a YYYYmmdd string.
>    - `api_instance.get_concept_preferred_term_for_id_using_get(concept_id, language, version)`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.
- Note that while it is possible to use `language` as a proxy for edition, it is not recommended.

### Get the normal form of a concept rendered in compositional grammar
> - Get normal form for concept for id 22298006 - Myocardial infarction , rendered in compositional grammar syntax - SNOMED CT's official syntax

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
# Getting the compositional grammar form of concept with given id
api_instance = swagger_client.ConceptresourceApi(api_client)
id = 22298006 # int | id
version =20160401 # str | The version of the terminology product as date string

try:
    # get the compositional grammar form of concept with given id
     api_response = api_instance.get_compositional_grammar_form_for_id_using_get(id, version)
     pprint(api_response)
except ApiException as e:
     print ("Exception when calling ConceptresourceApi->get_compositional_grammar_form_for_id_using_get: %s\n" % e)
```

> - The generic form of this query is below, where `concept_id` is the `id` of the concept and `version` is the edition to lookup expressed as a YYYYmmdd string.
>    - `api_instance.get_compositional_grammar_form_for_id_using_get(concept_id, version)`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.


### Compare two concepts to see if they are equivalent or if one subsumes the other
> - **Task** Compare two concepts for parent child relationship (child concept is asthma)

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
# Getting Subsumption relationship (comparing) two concepts with given id
api_instance = swagger_client.ConceptresourceApi(api_client)
concept_id1 = 64572001 # str | conceptId1
concept_id2 = 195967001 # str | conceptId2
version = 20160401 # str | The version of the terminology product as date string (optional)

try:
    # get the subsumption relationship of comparing two concepts
    api_response = api_instance.get_subsumption_relationship_using_get(concept_id1, concept_id2, version=version)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling ConceptresourceApi->get_subsumption_relationship_using_get: %s\n" % e)
```

> - The generic form of this query is below, where `concept_id1` and `concept_id2` are the `id`s of the concepts being compared and `version` is the edition to lookup expressed as a YYYYmmdd string.
>    - `api_instance.get_subsumption_relationship_using_get(concept_id1, concept_id2, version=version)` 
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.


## How to search for matches in Termlex

Warning: Make sure you have the correct API key before you try any of the other sections. Set the `Bearer token` as described above before you try the example.

### Get matches for token ast
In this example, we will search all of SNOMED CT content for matches for token `ast`. Termlex makes searching all of SNOMED CT trivial. 
> - **Task** Get matches for token 'ast' - returns first 100 results

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
api_instance = swagger_client.SearchcontrollerApi(api_client)
token = 'ast' # str | Token to search for; can be a term or an id

try:
    # Returns matches for given token for a specified version of SNOMED CT as a list of map objects
    api_response = api_instance.search_snomed_using_get(token)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling SearchcontrollerApi->search_snomed_using_get: %s\n" % e)
```

> - The general pattern for search is below, where `$token`is the query token
>    - `api_instance.search_snomed_using_get(token)`

Now let us analyse this query to understand what happened.
- First Termlex recognised that the search needed to be performed against SNOMED CT based on the URL of the GET query.
- Since we did not specify the `edition` and `version` of SNOMED CT to search against, it executed the search against the `default` version set up in Termlex. This is a useful fallback mechanism when you do not want to do a quick search. You can find ask Termlex for the `default version` information by visiting https://api.termlex.com/defaultVersion.
- Since we did not specify the number of matches to be returned, we were presented with the first 100 matches.
- The results is an array of objects, with addition information about `total number of matches`, `number of categories`, etc.

### Get matches for partial words
> - **Task** Get matches for partial words - aka incremental search

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
api_instance = swagger_client.SearchcontrollerApi(api_client)
token = 'dia mell' # str | Token to search for; can be a term or an id

try:
    # Returns matches for given token for a specified version of SNOMED CT as a list of map objects
    api_response = api_instance.search_snomed_using_get(token)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling SearchcontrollerApi->search_snomed_using_get: %s\n" % e)
```

- This query is almost the same as above, except we have used partial phraes to get matches. So instead of searching for `diabetes mellitus`, we specifed the search token as `dia melli`. 
- As in the case above, Termlex makes assumptions about other search parameters and returns matches as an array of matches.

## How to retrieve/expand concept hierarchies in Termlex

As you probably know by now, SNOMED CT's concepts/codes are organised as hierarchies. In order words, SNOMED CT is organised as a directed acyclical graph, where nodes in the graph are connected to each other. One of the common uses cases for a terminology server is to access children or parents of a given concept/code. So given the hierarchy of concepts below, the assocations between the concepts are given specific names in SNOMED CT.
```
        [A]
      /    \
    /       \
  [B]       [C]
           /  \
          /     \
        [D]     [E]
```
|Reference Node | Relationship name | Nodes                |
|:--------------|:------------------|:---------------------|
|[A]            | Children          | [B] and [C]          |
|[C]            | Children          | [D] and [E]          |
|[B]            | Parents           | [A]                  |
|[E]            | Parents           | [C]                  |
|[A]            | Descendants       | [B], [C], [D] and [E]|
|[D]            | Ancestors         | [C] and [A]          |

### Get children for a given concept
This call is useful when you want to only want to first level of child relationships for a given concept. In general this is useful when rendering a heirarchy.
> - **Task** Return children for diabetes mellitus

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
api_instance = swagger_client.HierarchyresourceApi(api_client)
id = 73211009 # str | id
version =  20160401 # str | The version of the terminology product as date string

try:
    # get the children of concept with given id
    api_response = api_instance.get_children_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling HierarchyresourceApi->get_children_for_id_using_get: %s\n" % e)
```

> - The generic form of this query is below, where `concept_id` is the `id` of the concept
>    - `api_instance.get_children_for_id_using_get(concept_id, version)`


#### Getting children for concept with labels
The above call only returns the concept ids of the children, forcing us to make another call to get the preferred terms of these concepts. This is particularly the case if we wanted to render the children for humans. Termlex provides a single call that returns both the ids and labels together.

> - **Task** Return children for diabetes  with labels
>    - `curl_get $baseUrl/hierarchy/73211009/childHierarchy`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance and $conceptId is the `id` of the concept
>    - `curl_get $baseUrl/hierarchy/$conceptId/childHierarchy`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.

### Get parents for a given concept
This call is useful when you want to only want to first level of parent relationships for a given concept. In general this is useful when rendering a heirarchy.
> - **Task** Return parents for diabetes mellitus

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
api_instance = swagger_client.HierarchyresourceApi(api_client)
id = 73211009 # str | id
version = 20160401 # str | The version of the terminology product as date string
definition_status = 'UNKNOWN' # str | definitionStatus (optional)

try:
    # get the parents of concept with given id
    api_response = api_instance.get_parents_for_id_using_get(id, version, definition_status=definition_status)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling HierarchyresourceApi->get_parents_for_id_using_get: %s\n" % e)
```

> - The generic form of this query is below, where `concept_id` is the `id` of the concept, `version` is the version of the edition to use and `definition_status` is the definition status we want to filter the parents with.
>    - `api_instance.get_parents_for_id_using_get(concept_id, version, definition_status=definition_status)`

#### Getting parents for concept with labels
The above call only returns the concept ids of the parents, forcing us to make another call to get the preferred terms of these concepts. This is particularly the case if we wanted to render the parents for humans. Termlex provides a single call that returns both the ids and labels together.
> - Return parents for diabetes  with labels
>    - `curl_get $baseUrl/hierarchy/73211009/parentHierarchy`
> - The generic form of this query is below, where $baseUrl is the URL of the Termlex instance and $conceptId is the `id` of the concept
>    - `curl_get $baseUrl/hierarchy/$conceptId/parentHierarchy`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.
- Note that concepts in SNOMED CT can have more than one parent. So this call returns all parents.

### Get descendants for a given concept
This call is useful when you want all hierarchy of a concept including its children, grandchildren, and so on. This call is useful when doing validations or querying. For example, in order to retrieve all patients with any heart disease, you would retrieve all descendants of heart disease and then select any patient records that contain any of the descendants.

> - **Task** Return descendants for diabetes mellitus

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
api_instance = swagger_client.HierarchyresourceApi(api_client)
id = 73211009 # str | id
version = 20160401 # str | The version of the terminology product as date string

try:
    # get the descendants of concept with given id
    api_response = api_instance.get_descendants_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling HierarchyresourceApi->get_descendants_for_id_using_get: %s\n" % e)
```

> - The generic form of this query is below, where `concept_id` is the `id` of the concept, `version` is the version of the edition to use
>    - `api_instance.get_descendants_for_id_using_get(concept_id, version)`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.

### Get ancestors for a given concept
This call is useful when you want all parent hierarchy of a concept including its parents, grandparents, and so on. This call is useful when doing validations.

> - **Task** Return ancestors for diabetes mellitus

Use the code below. The same code is also present in the [termlex_tutorial.py](/.termlex_tutorial.py) script in this folder.

```
api_instance = swagger_client.HierarchyresourceApi(api_client)
id = 73211009 # str | id
version = 20160401 # str | The version of the terminology product as date string
definition_status = 'UNKNOWN' # str | definitionStatus (optional)

try:
    # get the ancestors of concept with given id
    api_response = api_instance.get_ancestors_for_id_using_get(id, version, definition_status=definition_status)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling HierarchyresourceApi->get_ancestors_for_id_using_get: %s\n" % e)
```

> - The generic form of this query is below, where `concept_id` is the `id` of the concept, `version` is the version of the edition to use and `definition_status` is the definition status we want to filter the ancestors with.
>    - `api_instance.get_ancestors_for_id_using_get(id, version, definition_status=definition_status)`
- Note that in the absence of any version information, Termlex uses the `default` version loaded into it.

# Advanced Features - Constraints, Queries and Refsets

Please refer to the `curl` examples provided, while we work on expanding this section.

TBC