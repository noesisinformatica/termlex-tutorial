import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
import os

## Read TLX_USERNAME and TLX_PASSWORD variables set in environment variables
tlx_username = os.environ['TLX_USERNAME']
tlx_password = os.environ['TLX_PASSWORD']


## First configure the client library to communicate with an instance of Termlex
configuration = swagger_client.Configuration()
configuration.host = 'https://uat.termlex.com'
api_client = swagger_client.ApiClient(configuration)

# create an instance of the AccountResource API, to get token
api_instance = swagger_client.AccountresourceApi()
login_dto = swagger_client.LoginDTO() # LoginDTO | loginDTO
login_dto.username = tlx_username
login_dto.password = tlx_password

# Attempt to get API key
try: 
    # authorize
    api_response = api_instance.authorize_using_post(login_dto)
    key = api_response['id_token']
except ApiException as e:
    print("Exception when calling AccountresourceApi->authorize_using_post: %s\n" % e)

# Now set key as header value for all further access
print("\nUsing API Key: %s\n" %key)
api_client = swagger_client.ApiClient(header_name='Authorization', header_value='Bearer '+key)

# Getting account details
api_instance = swagger_client.AccountresourceApi(api_client)

try: 
    # getAccount
    api_response = api_instance.get_account_using_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AccountresourceApi->get_account_using_get: %s\n" % e)


# Getting concept for given id
api_instance = swagger_client.ConceptresourceApi(api_client)
concept_id = 22298006 # int | id The concept id
version = 'null' # str | The version of the terminology product as date string
flavour = 'ID_ONLY' # str | The flavour of the concept - specifies details retrieved (optional)

try: 
    # get the concept with given id
    api_response = api_instance.get_concept_for_id_using_get(concept_id, version, flavour=flavour)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConceptresourceApi->get_concept_for_id_using_get: %s\n" % e)


# Getting descriptions and relationships of the concept for given id
api_instance = swagger_client.ConceptresourceApi(api_client)
concept_id = 22298006 # int | id The concept id
version = 'null' # str | The version of the terminology product as date string
flavour = 'ID_DESCRIPTIONS_RELATIONSHIPS' # str | The flavour of the concept - specifies details retrieved (optional)

try:
    # get descriptions and relationships of the concept with given id
    api_response = api_instance.get_concept_for_id_using_get(concept_id, version, flavour=flavour)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConceptresourceApi->get_concept_for_id_using_get: %s\n" % e)



# Getting preferred term for a concept with given id
api_instance = swagger_client.ConceptresourceApi(api_client)
id = 22298006 # str | id
language = 'en' # str | language
version = 20160401 # str | The version of the terminology product as date string

try:
    # get the Preferred Term of concept with given id
     api_response = api_instance.get_concept_preferred_term_for_id_using_get(id, language, version)
     pprint(api_response)
except ApiException as e:
     print ("Exception when calling ConceptresourceApi->get_concept_preferred_term_for_id_using_get: %s\n" % e)


# Getting the compositional grammar form of concept with given id
api_instance = swagger_client.ConceptresourceApi(api_client)
id = 22298006 # int | id
version =20160401 # str | The version of the terminology product as date string

try:
    # get the compositional grammar form of concept with given id
     api_response = api_instance.get_compositional_grammar_form_for_id_using_get(id, version)
     pprint(api_response)
except ApiException as e:
     print ("Exception when calling ConceptresourceApi->get_compositional_grammar_form_for_id_using_get: %s\n" % e)



# Getting Subsumption relationship (comparing) two concepts with given id
api_instance = swagger_client.ConceptresourceApi(api_client)
concept_id1 = 64572001 # str | conceptId1
concept_id2 = 195967001 # str | conceptId2
version = 20160401 # str | The version of the terminology product as date string (optional)

try:
    # get the subsumption relationship of comparing two concepts
    api_response = api_instance.get_subsumption_relationship_using_get(concept_id1, concept_id2, version=version)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling ConceptresourceApi->get_subsumption_relationship_using_get: %s\n" % e)


## Getting matches for `ast` using default parameters // defaults to max 100 matches and default SNOMED CT edition in the API
api_instance = swagger_client.SearchcontrollerApi(api_client)
token = 'ast' # str | Token to search for; can be a term or an id

try:
    # Returns matches for given token for a specified version of SNOMED CT as a list of map objects
    api_response = api_instance.search_snomed_using_get(token)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling SearchcontrollerApi->search_snomed_using_get: %s\n" % e)


#Getting matches for token 'ast' using advanced parameters, restricting matches to `findings` hierarchy.
api_instance = swagger_client.SearchcontrollerApi(api_client)
token = 'ast' # str | Token to search for; can be a term or an id
max_results_size = 10 # int | Maximum number of results to return (optional) (default to 100)
start = 0 # int | The start position of search results to return (optional) (default to 0)
concept_types = ['404684003'] # list[str] | The concept hierarchies/types to filter results to (optional)

try:
    # Returns matches for given token for a specified version of SNOMED CT as a list of map objects
    api_response = api_instance.search_snomed_using_get(token, max_results_size=max_results_size, start=start, concept_types=concept_types)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling SearchcontrollerApi->search_snomed_using_get: %s\n" % e)



# Getting the children of concept with given id
api_instance = swagger_client.HierarchyresourceApi(api_client)
id = 73211009 # str | id
version =  20160401 # str | The version of the terminology product as date string

try:
    # get the children of concept with given id
    api_response = api_instance.get_children_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling HierarchyresourceApi->get_children_for_id_using_get: %s\n" % e)


#Getting parents for a concept with given id
api_instance = swagger_client.HierarchyresourceApi(api_client)
id = 73211009 # str | id
version = 20160401 # str | The version of the terminology product as date string
definition_status = 'UNKNOWN' # str | definitionStatus (optional)

try:
    # get the parents of concept with given id
    api_response = api_instance.get_parents_for_id_using_get(id, version, definition_status=definition_status)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling HierarchyresourceApi->get_parents_for_id_using_get: %s\n" % e)



#Getting descendants for a concept with given id
api_instance = swagger_client.HierarchyresourceApi(api_client)
id = 73211009 # str | id
version = 20160401 # str | The version of the terminology product as date string

try:
    # get the descendants of concept with given id
    api_response = api_instance.get_descendants_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling HierarchyresourceApi->get_descendants_for_id_using_get: %s\n" % e)



#Getting ancestors for a concept with given id
api_instance = swagger_client.HierarchyresourceApi(api_client)
id = 73211009 # str | id
version = 20160401 # str | The version of the terminology product as date string
definition_status = 'UNKNOWN' # str | definitionStatus (optional)

try:
    # get the ancestors of concept with given id
    api_response = api_instance.get_ancestors_for_id_using_get(id, version, definition_status=definition_status)
    pprint(api_response)
except ApiException as e:
    print ("Exception when calling HierarchyresourceApi->get_ancestors_for_id_using_get: %s\n" % e)



